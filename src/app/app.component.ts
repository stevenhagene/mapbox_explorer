import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  @Output() onDataSelected: any = new EventEmitter();
  dataPassed = 'all';
  title = 'mapboxApp';

  useData(data: string): any {
    if (data === 'all') {
      this.onDataSelected.emit(data);
    }
  }
}
