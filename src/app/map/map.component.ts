import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  // constructor(private map: MapService) {}
  constructor() {}

  map!: mapboxgl.Map;
  marker!: mapboxgl.Marker;
  popup!: mapboxgl.Popup;
  companies = [
    {
      companyName: 'Beroe Inc.',
      companyClassification: 'Software',
      companyAddress: 'Research IV, 909 Capability Drive, Raleigh, NC 27695, United States of America',
      siteAddress: 'Research IV, 909 Capability Drive, Raleigh, NC 27695, United States of America',
      companyDuns: 357489306,
      lng: -78.6805635,
      lat: 35.7722579,
    },
    {
      companyName: 'Red Hat',
      companyClassification: 'Software',
      companyAddress: '100 E Davie St, Raleigh, NC 27601',
      siteAddress: '100 E Davie St, Raleigh, NC 27601',
      companyDuns: 603984753,
      lng: -78.6378566,
      lat: 35.7751616,
    },
    {
      companyName: 'Sapiens',
      companyClassification: 'Software',
      companyAddress: '801 Corporate Center Dr #320, Raleigh, NC 27607',
      siteAddress: '801 Corporate Center Dr #320, Raleigh, NC 27607',
      companyDuns: 528908420,
      lng: -78.739578,
      lat: 35.792835,
    },
    {
      companyName: 'Dropsource',
      companyClassification: 'Software',
      companyAddress: '414 Fayetteville St, Raleigh, NC 27601',
      siteAddress: '414 Fayetteville St, Raleigh, NC 27601',
      companyDuns: 339425033,
      lng: -78.6397324,
      lat: 35.7751763,
    },
    {
      companyName: 'SMD Software Inc',
      companyClassification: 'Software',
      companyAddress: '3000 Highwoods Blvd, Raleigh, NC 27604',
      siteAddress: '3000 Highwoods Blvd, Raleigh, NC 27604',
      companyDuns: 339425033,
      lng: -78.6029994,
      lat: 35.8200497,
    },
  ];
  ngOnInit(): any {
    (mapboxgl as any).accessToken = 'pk.eyJ1Ijoic3RldmVuaGFnZW5lIiwiYSI6ImNraGtvNmozMzBydXgyd3BmenBoaWtmY3EifQ.HZllsXUD5-QgDmRq5IMs7Q';
    this.map = new mapboxgl.Map({
      container: 'mapbox',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-78.6805635, 35.7722579],
      zoom: 13,
    });
    this.map.addControl(new mapboxgl.NavigationControl());
    for (let i = 0; i < this.companies.length; i++) {
      const el = document.createElement('div');
      const obj = this.companies[i];
      el.id = 'marker' + obj.companyName;
      const myLatlng = new mapboxgl.LngLat(obj.lng, obj.lat);
      const marker = new mapboxgl.Marker()
        .setLngLat(myLatlng)
        .setPopup(
          new mapboxgl.Popup({ offset: 25 }).setHTML(`<div class="map-details-container flex-container-column">
          <div class="flex-container-row">
              <span class="overview-text">${obj.companyName} Overview</span>
          </div>
          <div style="padding-top: 20px;"></div>
          <div class="flex-container-row">
            <div class="flex-container-column">
              <div class="flex-container-row">
                <span class="content-text">BUSINESS NAME</span>
              </div>
              <div class="flex-container-row">
              <span class="company-text">${obj.companyName}</span>
              </div>
            </div>
            <div class="flex-container-column" style="padding-left:150px;">
              <div class="flex-container-row">
                <span class="content-text">BUSINESS CLASSIFICATION</span>
              </div>
              <div class="flex-container-row">
              <span class="company-text">${obj.companyClassification}</span>
              </div>
            </div>
          </div>
          <div style="padding-top: 20px;"></div>
          <div class="flex-container-row">
            <span class="content-text">DUNS #</span>
          </div>
          <div class="flex-container-row">
            <span class="company-text">${obj.companyDuns}</span>
          </div>
          <div style="padding-top: 20px;"></div>
          <div class="flex-container-row">
            <span class="content-text">Company Address</span>
          </div>
          <div class="flex-container-row">
            <span class="company-text">${obj.companyAddress}</span>
          </div>
          <div style="padding-top: 20px;"></div>
          <div class="flex-container-row">
            <span class="content-text">Site Address #</span>
          </div>
          <div class="flex-container-row">
            <span class="company-text">${obj.siteAddress}</span>
          </div>
        </div>`)
        )
        .addTo(this.map);
    }
  }
}
