import { Injectable, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
@Injectable({
  providedIn: 'root',
})
export class MapService {
  map!: mapboxgl.Map;
  startingLocation = [-77.0353, 38.8895];
  style = 'mapbox://styles/mapbox/streets-v11';
  lng = -77.032;
  lat = 38.913;
  zoom = 8;
  // geojson = {
  //   type: 'FeatureCollection',
  //   features: [
  //     {
  //       type: 'Feature',
  //       geometry: {
  //         type: 'Point',
  //         coordinates: [-77.032, 38.913],
  //       },
  //       properties: {
  //         title: 'Mapbox',
  //         description: 'Washington, D.C.',
  //       },
  //     },
  //     {
  //       type: 'Feature',
  //       geometry: {
  //         type: 'Point',
  //         coordinates: [-122.414, 37.776],
  //       },
  //       properties: {
  //         title: 'Mapbox',
  //         description: 'San Francisco, California',
  //       },
  //     },
  //   ],
  // };
  constructor() {}
  buildMap(): any {
    this.map = new mapboxgl.Map({
      accessToken: 'pk.eyJ1Ijoic3RldmVuaGFnZW5lIiwiYSI6ImNraGtvNmozMzBydXgyd3BmenBoaWtmY3EifQ.HZllsXUD5-QgDmRq5IMs7Q',
      container: 'map',
      style: this.style,
      zoom: this.zoom,
      center: [this.lng, this.lat],
    });
    // this.map.addControl(new mapboxgl.NavigationControl());
    let geojson = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            time: '2020-06-19T09:47:13Z',
            title: '10:47:13',
            description: '19 Jun 2020 at 10:47:13',
          },
          geometry: {
            type: 'Point',
            coordinates: [-2.219255366395865, 53.457215089777584],
          },
        },
        {
          type: 'Feature',
          properties: {
            time: '2020-06-19T09:47:19Z',
            title: 'home',
            description: '19 Jun 2020 at 10:47:19',
          },
          geometry: {
            type: 'Point',
            coordinates: [-2.219227672420135, 53.457351307993434],
          },
        },
      ],
    };
    // geojson.features.forEach(marker => {
    //   const el = document.createElement('div');
    //   el.className = 'marker';

    //   new mapboxgl.Marker(el)
    //     .setLngLat(marker.geometry.coordinates)
    //     .setLngLat(marker.geometry.coordinates)
    //     .setPopup(
    //       new mapboxgl.Popup({ offset: 25 }) // add popups
    //         .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>')
    //     )
    //     .addTo(map);
    // });
  }
  // adMarkers(): any {
  //   this.geojson.features.forEach((marker): any => {
  //     // create the popup
  //     const popup = new mapboxgl.Popup({ offset: 25 }).setText('Construction on the Washington Monument began in 1848.');
  //     const el = document.createElement('div');
  //     el.id = 'marker';
  //     new mapboxgl.Marker(el).setLngLat([marker.geometry.coordinates]).setPopup(popup).addTo(map);
  //     console.log(el);
  //   });
  // }
}
