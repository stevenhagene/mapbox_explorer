import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-heatmap',
  templateUrl: './heatmap.component.html',
  styleUrls: ['./heatmap.component.scss'],
})
export class HeatmapComponent implements OnInit, AfterViewInit {
  constructor() {}

  map!: mapboxgl.Map;
  marker!: mapboxgl.Marker;
  popup!: mapboxgl.Popup;
  ngOnInit(): void {
    (mapboxgl as any).accessToken = 'pk.eyJ1Ijoic3RldmVuaGFnZW5lIiwiYSI6ImNraGtvNmozMzBydXgyd3BmenBoaWtmY3EifQ.HZllsXUD5-QgDmRq5IMs7Q';
    // (mapboxgl as any).accessToken = 'AT';
    const bounds = [
      [-74.04728500751165, 40.68392799015035], // Southwest coordinates
      [-73.91058699000139, 40.87764500765852], // Northeast coordinates
    ];
    this.map = new mapboxgl.Map({
      container: 'mapbox',
      style: 'mapbox://styles/mapbox/dark-v10',
      center: [-78.6805635, 35.7722579],
      zoom: 2,
    });
    this.map.addControl(new mapboxgl.NavigationControl());
    this.map.on('style.load', () => {
      this.map.addSource('earthquakes', {
        type: 'geojson',
        // Point to GeoJSON data.
        data: '../../assets/earthquakes_all.geojson',
        cluster: true,
        clusterMaxZoom: 14, // Max zoom to cluster points on
        clusterRadius: 50, // Radius of each cluster when clustering points (defaults to 50)
      });

      this.map.addLayer({
        id: 'clusters',
        type: 'circle',
        source: 'earthquakes',
        filter: ['has', 'point_count'],
        paint: {
          // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
          // with three steps to implement three types of circles:
          //   * Blue, 20px circles when point count is less than 100
          //   * Yellow, 30px circles when point count is between 100 and 750
          //   * Pink, 40px circles when point count is greater than or equal to 750
          'circle-color': ['step', ['get', 'point_count'], '#51bbd6', 100, '#f1f075', 750, '#f28cb1'],
          'circle-radius': ['step', ['get', 'point_count'], 20, 100, 30, 750, 40],
        },
      });

      this.map.addLayer({
        id: 'cluster-count',
        type: 'symbol',
        source: 'earthquakes',
        filter: ['has', 'point_count'],
        layout: {
          'text-field': '{point_count_abbreviated}',
          'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
          'text-size': 12,
        },
      });

      this.map.addLayer({
        id: 'unclustered-point',
        type: 'circle',
        source: 'earthquakes',
        filter: ['!', ['has', 'point_count']],
        paint: {
          'circle-color': '#11b4da',
          'circle-radius': 4,
          'circle-stroke-width': 1,
          'circle-stroke-color': '#fff',
        },
      });
      this.map.on('click', 'cluster', e => {
        const features = this.map.queryRenderedFeatures(e.point, { layers: ['clusters'] });
        const clusterId = features[0].properties!.cluster_id;
        console.log(features[0].properties!.cluster_id);
        this.map.getSource('earthquakes').getClusterExpansionZoom(clusterId, (err: any, zoom: any) => {
          if (err) {
            return;
          }
          if (features[0].geometry.type === 'Point') {
            this.map.easeTo({
              center: features[0].geometry.coordinates,
              zoom,
            });
          }
          this.map.easeTo({
            center: features[0].geometry.coordinates,
            zoom,
          });
        });
      });

      this.map.on('click', 'clusters', e => {
        const features = this.map.queryRenderedFeatures(e.point, {
          layers: ['clusters'],
        });
        const clusterId = features[0].properties.cluster_id;
        this.map.getSource('earthquakes').getClusterExpansionZoom(clusterId, (err: any, zoom: any) => {
          if (err) {
            return;
          }
          // if (features[0].geometry.type === 'Point') {
          //   this.map.easeTo({
          //     center: features[0].geometry.coordinates,
          //     zoom,
          //   });
          // }
          this.map.easeTo({
            center: features[0].geometry.coordinates,
            zoom,
          });
        });
      });

      // When a click event occurs on a feature in
      // the unclustered-point layer, open a popup at
      // the location of the feature, with
      // description HTML from its properties.
      this.map.on('click', 'unclustered-point', e => {
        const coordinates = e.features[0].geometry.coordinates.slice();
        const mag = e.features[0].properties.mag;
        let tsunami;

        if (e.features[0].properties.tsunami === 1) {
          tsunami = 'yes';
        } else {
          tsunami = 'no';
        }

        // Ensure that if the map is zoomed out such that
        // multiple copies of the feature are visible, the
        // popup appears over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
          coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        new mapboxgl.Popup()
          .setLngLat(coordinates)
          .setHTML('magnitude: ' + mag + '<br>Was there a tsunami?: ' + tsunami)
          .addTo(this.map);
      });
    });
  }
  ngAfterViewInit(): any {}
}
