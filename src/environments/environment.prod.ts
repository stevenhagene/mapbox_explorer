export const environment = {
  production: true,
  mapbox: {
    accessToken: 'pk.eyJ1Ijoic3RldmVuaGFnZW5lIiwiYSI6ImNraGtvNmozMzBydXgyd3BmenBoaWtmY3EifQ.HZllsXUD5-QgDmRq5IMs7Q',
  },
};
